<?php
$img = imagecreatetruecolor(250, 250);

function imagefilledrotatedrectangle($img, $centerX, $centerY, $width, $height, $angle, $color) {
    $jobbfelsoX = $centerX + (($width/2) * cos($angle)) - (($height/2) * sin($angle));
    $jobbfelsoY = $centerY + (($width/2) * sin($angle)) + (($height/2) * cos($angle));
    
    $balfelsoX = $centerX - (($width/2) * cos($angle)) - (($height/2) * sin($angle));
    $balfelsoY = $centerY - (($width/2) * sin($angle)) + (($height/2) * cos($angle));
    
    $balalsoX = $centerX - (($width/2) * cos($angle)) + (($height/2) * sin($angle));
    $balalsoY = $centerY - (($width/2) * sin($angle)) - (($height/2) * cos($angle));
    
    $jobbalsoX = $centerX + (($width/2) * cos($angle)) + (($height/2) * sin($angle));
    $jobbalsoY = $centerY + (($width/2) * sin($angle)) - (($height/2) * cos($angle));
    
    imagefilledpolygon($img, array($jobbfelsoX, $jobbfelsoY, $balfelsoX, $balfelsoY, $balalsoX, $balalsoY, $jobbalsoX, $jobbalsoY), 4, $color);
}



//színek
$hatter = imagecolorallocate($img, 105, 217, 255);
$feher = imagecolorallocate($img, 255, 255, 255);
$fekete = imagecolorallocate($img, 0, 0, 0);
$piros = imagecolorallocate($img, 255, 0, 0);
$barna = imagecolorallocate($img, 165, 42, 42);

imagefill($img, 0, 0, $feher);
//rajzolás
imagefilledarc($img, 125, 125, 220, 220, 0, 0, $hatter, IMG_ARC_PIE); //háttér
    
//hóemberépítés
imagefilledarc($img, 125, 180, 90, 90, 0, 0, $feher, IMG_ARC_PIE);//alja
imagefilledarc($img, 125, 130, 70, 70, 0, 0, $feher, IMG_ARC_PIE);//közepe
imagefilledarc($img, 125, 90, 50, 50, 0, 0, $feher, IMG_ARC_PIE);//teteje

//gombok felrakása
$y = 210;
for($i = 0; $i<5; $i++){
    imagefilledarc($img, 125, $y, 7, 7, 0, 0, $fekete, IMG_ARC_PIE);
    $y -= 23;
}

//szemek
imagefilledarc($img, 115, 80, 7, 7, 0, 0, $fekete, IMG_ARC_PIE);
imagefilledarc($img, 135, 80, 7, 7, 0, 0, $fekete, IMG_ARC_PIE);

//száj
imagefilledarc($img, 125, 95, 12, 12, 0, 0, $piros, IMG_ARC_PIE);
imagefilledarc($img, 125, 93, 12, 12, 0, 0, $feher, IMG_ARC_PIE);

//bal kéz
imagefilledrotatedrectangle($img, 160, 115, 30, 3, -45, $barna);
imagefilledrectangle($img, 162, 110, 172, 111, $barna);
imagefilledrectangle($img, 161, 110, 160, 100, $barna);

//jobb kéz
imagefilledrotatedrectangle($img, 90, 115, 30, 3, 45, $barna);
imagefilledrectangle($img, 78, 110, 88, 111, $barna);
imagefilledrectangle($img, 88, 110, 89, 100, $barna);



header("Content-type: image/png");
imagepng($img);
imagedestroy($img);
?>